/*
Notas Version
La versión 2 funciona perfecto y entiendo que está bastante bien estructurada, en cuanto a la separación de funciones, etc.
En la versión 2.1 se separan las funciones de calculo y manejo de salida en otras hojas únicamente para mejor lectura.


Estoy viendo un breve parpadeo del LED testigo, no sé si realmente se prende, el solenoide no prende. 
Voy a hacer debug, para ver si es un tiempo muy chico o hay un encendido erroneo por soft.
Comento el running = 0; después del ON, está bien?

Sensor
https://sandboxelectronics.com/?product=100000ppm-mh-z16-ndir-co2-sensor-with-i2cuart-5v3-3v-interface-for-arduinoraspeberry-pi

Alimentacion para el sensor
                            Working voltage 4.5 V ~ 5.5V DC
                            Average current < 60mA（5VDC）
                            Peak current 150mA（5VDC）


Rango lectura 100000 ppm
Target 5% --> 50000 ppm

En la atmosfera actualmente constituye aproximadamente el 0,04 % (400 partes por millón)

Librería
https://github.com/SandboxElectronics/NDIR

Se usa la version I2C porque permite apagar el sensor por comandos. Esto es importante para que no se ejecute la auto calibracion.
Para esto hay que apagar el sensor al menos una vez cada 24hs. Ver de hacerlo con mayor frecuencia, si es que la condición de gas
es correcta. Es decir, no resetear en medio de un ajuste de concentracion.

Re-calibration of the sensor should be done in fresh air. After power on the sensor and put it in fresh air for at least 5 minutes. 
Either of following method could complete the re-calibration:

The manufacturer claims that the algorithm is effective against long time drift. This feature is good for applications which the sensor
will be in fresh air for at least some period of time in a cycle(24hours). For example, in your application, there is always sometime 
in a day that the CO2 concentration in the room is around 400ppm(theoretically, the CO2 concentration will never below 400ppm in fresh air).
However, it is a disaster if the sensor is in an atmosphere where the co2 concentration is always low or above 400ppm(for example,greenhouse).

To “break” the auto-calibration cycle, or to “disable” the auto-calibration, please turn OFF the sensor at least once every 24hours. 
User could turn ON/OFF the sensor by changing the state SC16IS750’s  PIN15 via I2C interface.


Meter una rutina de calibración en el menu. Aclarar que hay que ponerlo al aire libre al menos 5 minutos, preferentemente 20 minutos.


Regulador CO2 Talos
https://www.centralbier.com.ar/productos/regulador-co2-marca-talos-rosca-w21-8-2-relojes/
Adapatador a Drago
Puede ir una antirretorno entre la salida del regulador y la caja? Dentro de lo posible NO poner nada.

Conectores JG para entrada de gas a caja. Salidas?


Notas software:
Ver cual es el tiempo minimo de apertura del solenoide y el tiempo máximo sería con la cámara vacía y llegada a la presión 
de trabajo.

a) Hacer una rutina que cada X tiempo ejecute la medición
b) Evaluar la concentración actual y ajustar el tiempo de apertura del solenoide en función del error. Time proportional control


En caso de ser necesario un reset/disable de la calibración, ejecutar cada X horas!



*************************          Compras        **********************
Comprar 2 aireadores mas!!
2)  Componentes necesarios 
                               Solenoide 12V
                               Fuente 12V
                               Fan para recircular 12V
                               Regulador switching a 5V
                               Display
                               TIP122 para relay con un LED de testigo de estado de salida.
                               Arduino Nano muchos


- Pasa chapa x4 para salidas de mixed gas
- Pasa chapa para entrada de CO2
- Entrada de aire se hace con luer? 
- Salida del regulador de presion.
- Valvula aguja



*************                Logica del control           **************

ON time =  Sample Period * (Set point - Actual value)/Proportional band
OFF time =  Sample Period - ON time 
    
Sample period 20000 ms     Se recomienda que sea 1.5 veces el tiempo de respuesta del sistema a un cambio.  
Set point 45  °C
Proportional band 10  °C   temp a partir de la cual el controlador se encuentra totalmente activo.
Direction   

1) Cada segundo tomo los valores del sensor. Esa función devuelve el valor de "Input".
Dentro de esa función comparar con el "SetPoint" si se pasó interrumpir driveOutput.

2) La función driveOutput toma los valores y calcula OnTime y OffTime.
En esa rutina se evaluan tiempos mínimos/maximos de actuación.
Acá poner el flag que permite la espera para que termine la ejecución del anterior OnTime u OffTime.

3)

*/

#include "CThreadsLib.h"
#include <Wire.h>
#include "NDIR_I2C.h"
#include <LiquidCrystal_I2C.h>
#include "DHT.h"

LiquidCrystal_I2C lcd(0x27, 2, 1, 0, 4, 5, 6, 7);

byte smiley[8] = {
    B00000,
    B10001,
    B00000,
    B00000,
    B10001,
    B01110,
    B00000,
};

DHT dht(10, DHT22);

const int RELAY_PIN = 2;
float co2pc = 0;

boolean relayState = false;

long OnTime;
long OffTime;

long minOffTime = 300;
long minOnTime = 260;

float Input;
float SetPoint = 50000.00;
float SamplePeriod = 3000.00; //Mas de 12000 no llega nunca al set point, queda por debajo. Tarda mas en llegar pero menos overshoot.
// Probado con 4500 overshoot hasta 5.2
// Con 3000 funciona muy bien, baja muchísimo el overshoot, llega a 5.02 pero tarda mucho en llegar al set point.
float PropBand = 750.00; //Delta value in °C to the set point, when the output should stay 100% ON.

//SCFH out = 1
//SCFH in = ~> 0.1
//Sample period = 3000
//PropBand  = 750


//27del8
// SamplePeriod 3000
// PropBand 1100

bool running = 0;

unsigned long previousMillis = 0; // will store last time temp was updated
// constants won't change:
const long interval = 1000; // interval at which to request data (milliseconds)

NDIR_I2C mySensor(0x4D); //Adaptor's I2C address (7-bit, default: 0x4D)

void plotter()
{
  Serial.print(SetPoint);
  Serial.print(" ");
  Serial.print(Input);
  Serial.print(" ");
  //Serial.println(digitalRead(RELAY_PIN)*15);
}

template <typename... T>
void print(const char *str, T... args)
{
  int len = snprintf(NULL, 0, str, args...);
  if (len)
  {
    char buff[len];
    sprintf(buff, str, args...);
    Serial.println(buff);
  }
}

void printInfo(double co, double t, double h)
{

  //serial logg msg
  print("$%.1f %.1f %.1f;", co, t, h);

  // Serial.print("$");
  // Serial.print(co, 1);
  // Serial.print(" ");
  // Serial.print(t, 1);
  // Serial.print(" ");
  // Serial.print(h, 1);
  // Serial.println(";");

  //human infoirmation
  Serial.print(SetPoint);
  Serial.print("  Input  ");
  Serial.print((co), 3);

  Serial.print("   ");

  Serial.print(co, 1); //Esto se puede borrar
  Serial.print("%  ");

  Serial.print("  OutState  ");
  Serial.print(digitalRead(RELAY_PIN));
  Serial.print("        OnTime ");
  Serial.print(OnTime);
  Serial.print(" OffTime ");
  Serial.println(OffTime);

  //lcd display
  lcd.clear();
  lcd.home();
  lcd.print("Co2:");
  lcd.print(co, 1);
  lcd.print("% ");

  lcd.print("Out:");
  lcd.print(relayState ? "on" : "off");

  lcd.setCursor(0, 1);
  lcd.print("T:");
  lcd.print(t, 1);
  lcd.print("C ");

  lcd.print("H:");
  lcd.print(h, 0);
  lcd.print("% ");
}

void getSetPoint()
{
  if (Serial.available() > 0)
  {
    SetPoint = Serial.parseFloat();
    running = 0; // Tengo que poner el flag en 0 para que recalcule los tiempos.
    Serial.println("Nuevo SetPoint");
  }
}

int driveOut()
{
  //...
  // Inicio del thread
  CThreadBegin();

  // bucle infinito del thread
  while (1)
  {

    //getTime(); // No hace falta que lo haga acá si lo hago en el loop.
    digitalWrite(RELAY_PIN, HIGH);
    relayState = true;
    //Serial.print("Encendido  "); Serial.println(OnTime);
    CThreadSleep(OnTime);
    //running = 0;

    // Recalcular los tiempos? O no hace falta?

    //getTime(); // No hace falta que lo haga acá si lo hago en el loop.
    digitalWrite(RELAY_PIN, LOW);
    relayState = false;

    //Serial.print("Apagado   "); Serial.println(OffTime);
    CThreadSleep(OffTime);
    running = 0;
    //Serial.println(digitalRead(RELAY_PIN));

    //Serial.println("Fin secuencia");
  }

  // fin del thread
  CThreadEnd();
}

void calcOutput()
{

  if (running == 0)
  { //Si se está esperando que cambie la salida no se recalculan los tiempos.
    OnTime = SamplePeriod * ((SetPoint - Input) / PropBand);
    OffTime = SamplePeriod - OnTime;

    //Serial.print("ON: "); Serial.print(OnTime); Serial.print(" -  OFF: "); Serial.println(OffTime);
    Serial.println("Recalculo");

    //Mientras OFF time sea menor o igual a 0, Salida en ON
    //Mientras ON time sea menor o igual a 0, Salida en OFF

    if (OnTime <= 0)
    {
      OffTime = SamplePeriod;
      OnTime = 0;
    }

    if (OffTime <= 0)
    {
      OnTime = SamplePeriod;
      OffTime = 0;
    }

    // Estas dos condiciones limitan los tiempos de encendido y apagado a valores mínimos.

    if (OnTime <= minOnTime && OnTime > 0)
    {
      OnTime = minOnTime;
    }

    if (OffTime <= minOffTime && OffTime > 0)
    {
      OffTime = minOffTime;
    }

    running = 1;
  }

  if (running == 1)
  {
    driveOut();
  }
}

//*****************     Get concentration      ***********************
void getConcentration()
{
  unsigned long currentMillis = millis();

  if (currentMillis - previousMillis >= interval)
  {
    // save the last time you request temperature
    previousMillis = currentMillis;

    if (mySensor.measure())
    {
      //Serial.print(mySensor.ppm);
      //Serial.println(" ppm  ");
      co2pc = mySensor.ppm / 10000.00;
      Input = mySensor.ppm;
      //Serial.println((co2pc),3);
    }
    else
    {
      lcd.clear();
      lcd.home();
      lcd.print("ERROR! CO2");
      Serial.println("Sensor communication error.");
    }

    float h = dht.readHumidity();
    // Read temperature as Celsius (the default)
    float t = dht.readTemperature();

    //Serial.println("pido");
    printInfo(co2pc, t, h);
  }
}
//**********************  Get concentration END  ***************************

void setup()
{
  Serial.begin(9600);

  pinMode(RELAY_PIN, OUTPUT);
  digitalWrite(RELAY_PIN, LOW); // Iniciar con el relay cerrado.

  lcd.begin(16, 2); // initialize the lcd
  lcd.setBacklightPin(3, POSITIVE);
  lcd.setBacklight(HIGH);

  lcd.createChar(0, smiley);

  lcd.home(); // go home
  lcd.print("STAMM ATMOSPHERE");
  lcd.setCursor(0, 1);
  lcd.print("Iniciando");

  dht.begin();

  // delay(5000);

  int trys = 0;
  while (!mySensor.begin())
  {
    lcd.print(".");
    trys++;
    if (trys > 6)
    {
      lcd.clear();
      lcd.home();
      lcd.print("ERROR! CO2 INIT");
      while (1)
        ;
    }

    Serial.println("ERROR: Failed to connect to the sensor.");
    delay(2000);
  }

  Serial.println("Wait 10 seconds for sensor initialization...");

  // lcd.clear();
  lcd.setCursor(0, 1);
  lcd.print("Calibrando... ");

  for (int t = 10; t > 0; t--)
  {
    Serial.println(t);
    lcd.setCursor(14, 1);
    lcd.print(t - 1);
    delay(1000);
  }

  mySensor.disableAutoCalibration();
} //*********************      SET UP END   ***************************

// Puse set point en 777 cuando estaba en 770 el input y me quedó alto pero sin estar always on!!!
//

void loop()
{

  getSetPoint(); // Para ingresar SetPoint via terminal serie (No line ending).
  getConcentration();
  calcOutput();
  //printInfo(); // La corro dentro de getConcentration 1 vez por segundo
  //plotter();
}
//*********************       LOOP END     ***************************
